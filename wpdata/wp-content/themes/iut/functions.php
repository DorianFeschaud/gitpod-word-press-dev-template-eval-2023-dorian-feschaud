<?php

function iut_wp_enqueue_scripts() {

	$parenthandle = 'twentynineteen-style';

	$theme        = wp_get_theme();

    //Load parent CSS
	wp_enqueue_style(
        $parenthandle,
		get_template_directory_uri().'/style.css', //https://plop.org/wp-content/themes/twentynineteen/style.css
		array(), 
		$theme->parent()->get( 'Version' )
	);

    //Load child CSS (this theme)
	wp_enqueue_style(
        'iut-style',
		get_stylesheet_uri(), //https://plop.org/wp-content/themes/iut/style.css
		array( $parenthandle ),
		$theme->get( 'Version' )
	);
}
add_action( 'wp_enqueue_scripts', 'iut_wp_enqueue_scripts' );

function wporg_custom_post_type() {
	register_post_type('wporg_recipe',
		array(
			'labels'      => array(
				'name'          => __('Recettes', 'textdomain'),
				'singular_name' => __('Recette', 'textdomain'),
			),
			'public'      => true,
			'publicity_queryable' => true,
			'hierarchical' => true,
			'show_in_rest' => true,
			'supports' => array('title', 'editor', 'thumbnail', 'page-attributes'),
			'has_archive' => 'recettes',
			'rewrite' => array('slug' => 'recipe'),
		)
	);
}
add_action('init', 'wporg_custom_post_type');


function iut_add_meta_boxes_project() {
	
	add_meta_box(
		'iut_mbox_project',                 // Unique ID
		'Infos complémentaires',      // Box title
		'iut_mbox_project_content',  // Content callback, must be of type callable
		'recipe'                            // Post type
	);
}
add_action( 'add_meta_boxes', 'iut_add_meta_boxes_project' );

function iut_mbox_project_content( $post ) {

	$ingredients = get_post_meta(
		$post->ID,
		'ingredients',
		true
	);

	echo '<label for="ingredients">';
	echo 'Ingrédients : ';
	echo '<input type="text" id="ingredients" name="ingredients" value"'.$ingredients.'">';
	echo '</label>';
}

function iut_save_post( $post_ID ) {

	if ( isset($_POST['ingredients']) && !empty($_POST['ingredients'])) {
		update_post_meta(
			$post_ID,
			'ingredients',
			sanitize_text_field($_POST['ingredients'])
		);
	}
}

add_action('save_post', 'iut_save_post');